
//
//  Validator.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-18.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import UIKit

class Validator: NSObject {
    
    var prototypeDatabase = [1:"california", 2:"newyork", 3:"texas", 4:"washington", 5:"florida"]
    
    func validate(userAnswer:String, questionID:Int, clueNumber:Int) -> (Bool, [Int], String) {
        let stats = downloadCommunityData()
        let correctAnswer = downloadAnswerWith(ID: questionID)
        let str = reformat(userInput: userAnswer)
        if str == correctAnswer {
            uploadData(userAnswer: userAnswer, identifier: questionID, correct: true, clueNumber: clueNumber)
            return (true, stats, correctAnswer)
        } else {
            uploadData(userAnswer: userAnswer, identifier: questionID, correct: true, clueNumber: clueNumber)
            return (false, stats, correctAnswer)
        }
    }
    
    func reformat(userInput:String) -> String {
        var outputString = userInput.lowercased()
        outputString = outputString.replacingOccurrences(of: " ", with: "")
        outputString = outputString.replacingOccurrences(of: "the", with: "")
        outputString = outputString.replacingOccurrences(of: "and", with: "")
        outputString = outputString.replacingOccurrences(of: ".", with: "")
        return outputString
    }
    
    func downloadAnswerWith(ID:Int) -> String {
        if let answer =  prototypeDatabase[ID] {
            return answer
        } else {
            fatalError()
        }
    }
    
    func uploadData(userAnswer:String, identifier:Int, correct:Bool, clueNumber:Int) {
        // Upload user answer and whether it is correct to AWS
    }
    
    func downloadCommunityData() -> [Int]{
        return [15,25,32]
    }
    
}
