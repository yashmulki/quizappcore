//
//  Game.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-18.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import Foundation

struct Game:Codable {
    var questions:[Question]
    var identifier:Int
    var questionIndex:Int
    var clueIndex:Int
    var totalScore:Int
}
