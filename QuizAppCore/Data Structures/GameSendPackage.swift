//
//  GameSendPackage.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-19.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import Foundation

struct GameSendPackage:Codable {
    var answer:String
    var correctAnswer:String
    var stats:[Int]
    var correct:Bool
    var gameScore:Int
    var questionNumber:Int
    var gameStats:[Int]?
}
