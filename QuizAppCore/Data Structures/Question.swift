//
//  Question.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-18.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import Foundation

struct Question:Codable {
    var clues:[String]
    var id:Int
}
