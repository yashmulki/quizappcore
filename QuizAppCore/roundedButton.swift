//
//  roundedButton.swift
//  Quiz App
//
//  Created by Yashvardhan Mulki on 2017-06-10.
//  Copyright © 2017 QuantumPoint Development. All rights reserved.
//

import UIKit

@IBDesignable class roundedButton: UIButton {
    @IBInspectable var cornerRadius:CGFloat = 15.0
    @IBInspectable var color:UIColor = UIColor.white

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code

        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        color.setFill()
        color.setStroke()
        path.stroke()
        path.fill()

    }



}
