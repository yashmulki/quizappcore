//
//  ViewController.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-18.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // IB Outlets
    @IBOutlet var questionNumberLabel: UILabel!
    @IBOutlet var clueLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var answerField: UITextField!
    @IBOutlet var nextClueButton: roundedButton!
    
    @IBAction func editingStarted(_ sender: Any) {
        if !userHasTyped {
            userHasTyped = true
            timeRemaining += 15
        }
    }
    
    /// Creates a popup
    ///
    /// - Parameters:
    ///   - title: Popup Title
    ///   - subTitle: Popup Subtitle
    func popup(title:String, subTitle:String) {
        let ac = UIAlertController(title: title, message: subTitle, preferredStyle: .alert)
        let acAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        ac.addAction(acAction)
        self.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: Any) {
        if answerField.text != nil {
            let (value, stats, correctAnswer) = validator.validate(userAnswer: answerField.text!, questionID: currentGame.questions[currentGame.questionIndex].id, clueNumber: currentGame.clueIndex + 1)
            if value {
                print("correct")
                currentGame.totalScore += 6 - currentGame.clueIndex
                if currentGame.questionIndex == currentGame.questions.count - 1 {
                    //popup(title: "Correct-Game Over", subTitle: "Your Final Score is: \(currentGame.totalScore)")
                } else {
                    //popup(title: "Correct", subTitle: "Your Score is: \(currentGame.totalScore)")
                }
                displayResult(userAnswer: answerField.text!, correctAnswer: correctAnswer, stats: stats, correct: true)
            } else {
                print("wrong")
                if currentGame.questionIndex == currentGame.questions.count - 1 {
                    popup(title: "Wrong-Game Over", subTitle: "Your Final Score is: \(currentGame.totalScore)")
                } else {
                //    popup(title: "Wrong", subTitle: "Your Score is: \(currentGame.totalScore)")
                }
                 displayResult(userAnswer: answerField.text!, correctAnswer: correctAnswer, stats: stats, correct: false)
            }
        }
        if currentGame.questionIndex == currentGame.questions.count - 1 {
            gameIsOver = true
                UserDefaults.standard.removeObject(forKey: "saveGame")
        }
        nextQuestion()
    }

    @IBAction func nextClue(_ sender: Any) {
        nextClue()
        if currentGame.clueIndex == currentGame.questions[currentGame.questionIndex].clues.count - 1 {
            nextClueButton.isEnabled = false
        }
    }

    // Game Variables
    var currentGame:Game!
    var validator:Validator!
    var userHasTyped = false
    var timeRemaining = 20
    var clueTimer:Timer!
    var gameSendPackage:GameSendPackage!
    var gameIsOver = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupGame()
        validator = Validator()
        answerField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController {

    func updateUserInterface() {
      questionNumberLabel.text = "Trivia Riddle \(currentGame.questionIndex + 1) of \(currentGame.questions.count)"
      clueLabel.text = "Clue #\(currentGame.clueIndex + 1): \(currentGame.questions[currentGame.questionIndex].clues[currentGame.clueIndex]) [\(6 - currentGame.clueIndex) Points]"
      timeLabel.text = "\(timeRemaining) Seconds"
        if !gameIsOver {
            let encoder = PropertyListEncoder()
            var data:Data
            do{
                data = try encoder.encode(currentGame)
                UserDefaults.standard.set(data, forKey: "saveGame")
            }catch{
                print("Error3")
                return
            }
        }
    }

    func downloadQuestionSet() -> [Question] {
        let question1 = Question(clues: ["San Francisco", "Los Angeles", "Sunnyvale", "Los Angleles", "San Jose", "Fresno"], id: 1)
        let question2 = Question(clues: ["New York City", "Albany", "Buffalo", "Rochester", "Syracuse", "Elmira"], id: 2)
        let question3 = Question(clues: ["Austin", "Dallas", "Houston", "Forth Worth", "San Antonio", "Arlington"], id: 3)
        let question4 = Question(clues: ["Seattle", "Tacoma", "Bellevue", "Redmond", "Everett", "Spokane"], id: 4)
        let question5 = Question(clues: ["Miami", "Orlando", "Tampa", "Key West", "Sarasota", "Fort Lauderdale"], id: 5)
        return[question1, question2, question3, question4, question5]
    }

    func setupGame()   {
        if let save = UserDefaults.standard.object(forKey: "saveGame") as? Game {
            currentGame = save
            updateUserInterface()
        } else {
            currentGame = Game(questions: downloadQuestionSet(), identifier: 1, questionIndex: 0, clueIndex: 0, totalScore: 0)
            updateUserInterface()
        }
        clueTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerSelector), userInfo: nil, repeats: true)
    }
    
    @objc func timerSelector() {
        timeRemaining -= 1
        updateUserInterface()
        if timeRemaining == 0 {
            if currentGame.clueIndex == currentGame.questions[currentGame.questionIndex].clues.count {
                nextQuestion()
            } else {
                nextClue()
            }
        }
    }
    
    func nextClue() {
        if currentGame.clueIndex != currentGame.questions[currentGame.questionIndex].clues.count - 1{
            timeRemaining = 20
            currentGame.clueIndex += 1
            userHasTyped = false
            updateUserInterface()
        } else {
            nextQuestion()
        }
    }
    
    func nextQuestion() {
        if currentGame.questionIndex == currentGame.questions.count - 1 {
            // Nothing
            clueTimer.invalidate()
        } else {
            nextClueButton.isEnabled =  true
            timeRemaining = 20
            currentGame.clueIndex = 0
            userHasTyped = false
            currentGame.questionIndex += 1
            answerField.text = ""
            updateUserInterface()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        answerField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func returnCommunityQuizData(userScore:Int, identifier:Int) -> [Int] {
        // Fetch from AWS
        return [20, 50, 30]
    }
    
    func displayResult(userAnswer:String, correctAnswer:String, stats:[Int], correct:Bool) {
        if currentGame.questionIndex == currentGame.questions.count - 1 {
            gameSendPackage = GameSendPackage(answer: userAnswer, correctAnswer: correctAnswer, stats: stats, correct: correct, gameScore: currentGame.totalScore, questionNumber: currentGame.questionIndex + 1, gameStats: returnCommunityQuizData(userScore: currentGame.totalScore, identifier: currentGame.identifier))
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QuizResultViewController") as! QuizResultViewController
            vc.dataPackage = self.gameSendPackage
            self.present(vc, animated: true, completion: nil)
       
        } else {
            gameSendPackage = GameSendPackage(answer: userAnswer, correctAnswer: correctAnswer, stats: stats, correct: correct, gameScore: currentGame.totalScore, questionNumber: currentGame.questionIndex + 1, gameStats: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QuestionResultViewController") as! QuestionResultViewController
            vc.dataPackage = self.gameSendPackage
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "questionPerformance" || segue.identifier == ""{
            let destination = segue.destination as! QuestionResultViewController
            destination.dataPackage = self.gameSendPackage
        } else if segue.identifier == "quizPerformance" {
            let destination = segue.destination as! QuizResultViewController
            destination.dataPackage = self.gameSendPackage
        }
    }
    
}
