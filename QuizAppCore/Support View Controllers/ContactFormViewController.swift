
//
//  ContactFormViewController.swift
//  QuizApp
//
//  Created by Yashvardhan Mulki on 2018-02-15.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import UIKit

class ContactFormViewController: UIViewController{
    
    var contactMethodIsEmail = true
    
    // IB Outlets
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var contactSegment: UISegmentedControl!
    @IBOutlet var contactField: UITextField!
    @IBOutlet var commentField: UITextView!
    @IBOutlet var methodLabel: UILabel!
    
    // IB Actions
    
    @IBAction func segmentDidChange(_ sender: Any) {
        if contactMethodIsEmail {
            contactMethodIsEmail = false
            methodLabel.text = "Phone Number"
        } else {
            contactMethodIsEmail = true
            methodLabel.text = "E-Mail"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func submitDetails() {
        // Upload data from all fields to AWS
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
