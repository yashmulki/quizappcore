//
//  ViewController.swift
//  QuizApp
//
//  Created by Yashvardhan Mulki on 2018-02-11.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBAction func newChallenge(_ sender: Any) {
    }
    
    let appID = ""
    
    @IBAction func rateApp(_ sender: Any) {
        rateThisApp()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if UserDefaults.standard.object(forKey: "saveGame") != nil {
            performSegue(withIdentifier: "toGame", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// Support Functions
extension MenuViewController {
    
    
    func rateThisApp() {
        // https://stackoverflow.com/questions/27755069/how-can-i-add-a-link-for-a-rate-button-with-swift
        let appID = "343200656"
        
        if let checkURL = URL(string: "https://itunes.apple.com/app/id\(appID)") {
            open(url: checkURL)
        } else {
            print("invalid url")
        }
    }
    
    
    /// Opens a URL
    ///
    /// - Parameter url: URL to open
    func open(url: URL) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open \(url): \(success)")
            })
        } else if UIApplication.shared.openURL(url) {
            print("Open \(url)")
        }
    }
    
}

