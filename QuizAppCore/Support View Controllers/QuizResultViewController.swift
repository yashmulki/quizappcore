//
//  QuizResultViewController.swift
//  QuizAppCore
//
//  Created by Yashvardhan Mulki on 2018-02-19.
//  Copyright © 2018 qpdevelopment. All rights reserved.
//

import UIKit

class QuizResultViewController: UIViewController {

    let maxPoints = 10*6
    var dataPackage:GameSendPackage!
    var greenColor = UIColor(red: 46, green: 177, blue: 135, alpha: 1)
    var redColor = UIColor(red: 217, green: 56, blue: 41, alpha: 1)
    
    // IB Outlets
    @IBOutlet var statusView: UIView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var questionLabel: UILabel!
    
    @IBOutlet var userAnswer: UILabel!
    @IBOutlet var correctAnswer: UILabel!
    @IBOutlet var usersBeat: UILabel!
    @IBOutlet var usersTied: UILabel!
    @IBOutlet var usersLost: UILabel!
    @IBOutlet var score: UILabel!
    
    @IBOutlet var gameUsersBeat:UILabel!
    @IBOutlet var gameUsersTied:UILabel!
    @IBOutlet var gameUsersLost:UILabel!

    
    @IBAction func dismiss(_ sender: Any){
        self.performSegue(withIdentifier: "backHome", sender: self)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if dataPackage.correct {
            // statusImageView.image = #imageLiteral(resourceName: "CorrectIcon")
            statusView.backgroundColor = greenColor
            statusLabel.text = "Correct"
        } else {
            // Wrong Icon
            statusView.backgroundColor = UIColor.red
            statusLabel.text = "Incorrect"
        }
        
        userAnswer.text = "Your Answer: \(dataPackage.answer)"
        correctAnswer.text = "Correct Answer: \(dataPackage.correctAnswer)"
        score.text = "Score: \(dataPackage.gameScore) of \(maxPoints) Possible Points"
        usersBeat.text = "You beat \(dataPackage.stats[0]) of users"
        usersTied.text = "You tied \(dataPackage.stats[1]) of users"
        usersLost.text = "You lost to \(dataPackage.stats[2]) of users"
        questionLabel.text = "Trivia Riddle \(dataPackage.questionNumber) of \(maxPoints/6)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
